using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;

    [Header("Simulation Area")]
    public float leading = -30f;
    public float trailing = 30f;
    public float top = -13.5f;
    public float bottom = 13.5f;

    [Header("Car Setup")]
    public float speed = 4f;
    public GameObject carPrefab;

    [Header("Spawning Setup")]
    public int numbersOfCars = 100;
    public int carQty = 0;

    private UIManager myUIManager;
    private EntityManager entityManager;
    private Entity carEntityPrefab;
    private GameObjectConversionSettings settings;

    private void Awake()
    {
        if(GM != null && GM != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        myUIManager = FindObjectOfType<UIManager>();

        //Set this as the current game manager.
        GM = this;

        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        carEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(carPrefab, settings);
    }

    void CarIncrements()
    {
        if (Input.GetKeyDown(name: "space") && myUIManager.Fps >= 30)
            AddCars(numbersOfCars);
    }

    void AddCars(int amount)
    {
        for(int i = 0; i < amount; i++)
        {
            float xValue = Random.Range(top, bottom);
            float zValue = Random.Range(0f, 2f);

            Vector3 spawnPosition = new Vector3(xValue, 0.44f, zValue + leading);
            Quaternion spawnRotation = Quaternion.Euler(0f, 0f, 0f);

            //var obj = Instantiate(carPrefab, spawnPosition, spawnRotation) as GameObject;

            Entity car = entityManager.Instantiate(carEntityPrefab);
            Translation translation = new Translation();
            translation.Value = spawnPosition;
            entityManager.SetComponentData(car, translation);
        }

        //Increment the numbers of cars
        carQty += amount;
        UIManager.UpdateCarsUI(carQty);
    }

    private void Update()
    {
        CarIncrements();
    }


}
