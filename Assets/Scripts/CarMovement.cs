using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    void Movement()
    {
        var transform1 = transform;
        Vector3 pos = transform1.position;
        pos += Time.deltaTime * GameManager.GM.speed * transform1.forward;

        if (pos.z > GameManager.GM.trailing)
            pos.z = GameManager.GM.leading;

        transform1.position = pos;
    }

    private void Update()
    {
        Movement();
    }
}
