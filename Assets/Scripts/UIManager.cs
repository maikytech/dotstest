using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    //This class a static reference to itself to ensure that there will only be
    //one in exitence. This is often referred to as a singleton design pattern. Other
    //scripts access this one through its public static methods.

    static UIManager current;

    public TextMeshProUGUI carCountText;
    public TextMeshProUGUI fpsText;

    private int numberOfFrames = 0;
    private float deltaTime = 0.0f;
    private float fps = 0.0f;
    private float updateRate = 4.0f;

    public float Fps => fps;

    private void Awake()
    {
        if(current != null && current != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        current = this;
    }

    public static void UpdateCarsUI(int carCount)
    {
        if (current == null)
            return;

        current.carCountText.text = carCount.ToString();
    }

    public static void UpdateFpsUI()
    {
        if (current == null)
            return;

        current.numberOfFrames++;
        current.deltaTime += Time.deltaTime;

        if(current.deltaTime > 1.0 / current.updateRate)
        {
            current.fps = current.numberOfFrames / current.deltaTime;
            current.numberOfFrames = 0;
            current.deltaTime -= (1.0f / current.updateRate);
        }

        current.fpsText.text = current.fps.ToString(format: "000");
    }

    private void Update()
    {
        UpdateFpsUI();
    }
}
