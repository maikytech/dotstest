using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

//[AlwaysSynchronizeSystem]           //Main Thread
//public class CarMovementSystem : SystemBase
public class CarMovementSystem : JobComponentSystem
{
    //protected override void OnUpdate()
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = Time.DeltaTime;
        float leading = GameManager.GM.leading;
        float trailing = GameManager.GM.trailing;

        //Synchronous mode
        //ref = Write data, in = read data
        /*
        Entities.ForEach((ref Translation translation, in CarData carData) =>
        {
          
            translation.Value.z = (translation.Value.z + (carData.speed * deltaTime));
            if (translation.Value.z > trailing)
                translation.Value.z = leading;

        }).Run();

        return default;

        */

        //Asynchronous mode
        JobHandle jobHandle = Entities.ForEach((ref Translation translation, in CarData carData) =>
        {

            translation.Value.z = (translation.Value.z + (carData.speed * deltaTime));
            if (translation.Value.z > trailing)
                translation.Value.z = leading;

        }).Schedule(inputDeps);

        return jobHandle;

        //}).Schedule();
    }
}
